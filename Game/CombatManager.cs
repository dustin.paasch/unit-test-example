﻿namespace Game;

public class CombatManager
{
	private readonly List<Character> _characters = new();


	public void AddCharacter(Character character)
	{
		this._characters.Add(character);
	}


	/// <summary>
	/// Returns true as long as there is fighting happening.
	/// False means that a character won.
	/// </summary>
	public bool Round()
	{
		if (this._characters.Count == 1)
		{
			Console.WriteLine($"The only survivor is {this._characters[0].Name}");
			return false;
		}

		foreach (Character character in this._characters)
		{
			if (character.IsDead)
				continue;
			
			Console.WriteLine($"{character.Name}s turn...");
			Character targetCharacter = character.PickTarget(
				this._characters.FindAll(filteredCharacter => !filteredCharacter.IsDead)
			);
			
			Console.WriteLine($"{character.Name} targets {targetCharacter.Name}...");
			int damage = character.GetCurrentDamage();
			
			Console.WriteLine($"{character.Name} is dealing {damage} damage to {targetCharacter.Name}");
			bool isDead = targetCharacter.ApplyDamage(damage);
		}

		this._characters.RemoveAll(character => character.IsDead);

		return true;
	}
}
