﻿using Game;

Console.WriteLine("Welcome to the ultimate combat simulator!");

CombatManager combatManager = new();
combatManager.AddCharacter(new Player());
combatManager.AddCharacter(new Orc());
combatManager.AddCharacter(new Goblin());


int roundCounter = 1;
while (combatManager.Round())
{
	roundCounter++;
	Console.WriteLine($"\n\nNext round {roundCounter}!");
}

Console.WriteLine("Bye!");
