﻿namespace Game;

public abstract class Character
{
	public const string DefaultName = "Unkown";
	
	public Inventory Inventory { get; set; }
	public int Health { get; private set; } = 100;
	public bool IsDead { get; private set; }
	public string Name { get; protected init; } = DefaultName;


	public abstract Character PickTarget(List<Character> possibleTargets);

	public abstract int GetCurrentDamage();

	/// <summary>
	/// Returns if the character dies by the damage applied.
	/// </summary>
	public bool ApplyDamage(int damage)
	{
		this.Health -= damage;

		if (this.Health <= 0)
		{
			this.IsDead = true;
			this.Health = 0;
			Console.WriteLine($"{this.Name} died.");
			return true;
		}

		return false;
	}
}
