﻿namespace Game;

public class Player : Character
{
	public Player()
	{
		this.Name = "Player";
	}
	
	
	public override Character PickTarget(List<Character> possibleTargets)
	{
		Console.WriteLine("Possible targets:");
		int index = 0;
		foreach (Character possibleTarget in possibleTargets)
		{
			Console.WriteLine($"{index}:\t{possibleTarget.Name}");
			index++;
		}
		
		Console.WriteLine("Which target?");
		int targetIndex = Convert.ToInt32(Console.ReadLine());

		return possibleTargets[targetIndex];
	}

	public override int GetCurrentDamage()
	{
		return 10;
	}
}
