﻿namespace Game;

public class Enemy : Character
{
	private readonly Random _random = new();


	public override Character PickTarget(List<Character> possibleTargets)
	{
		int randomIndex = this._random.Next(possibleTargets.Count);
		return possibleTargets[randomIndex];
		
		// return possibleTargets[this._random.Next(possibleTargets.Count)];
	}

	public override int GetCurrentDamage()
	{
		return this._random.Next(8, 13);
	}
}
