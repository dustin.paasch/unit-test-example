using System.Reflection;
using Xunit.Abstractions;

namespace Tests;

public class TestClass
{
	private readonly ITestOutputHelper _testOutputHelper;

	public TestClass(ITestOutputHelper testOutputHelper)
	{
		this._testOutputHelper = testOutputHelper;
	}

	[Fact]
	public void TestPlayerDamageCalculation()
	{
		Player player = new();
		
		Assert.Equal(100, player.Health);
		Assert.False(player.IsDead);

		player.ApplyDamage(15);
		
		Assert.Equal(85, player.Health);
		Assert.False(player.IsDead);

		player.ApplyDamage(999);
		
		Assert.Equal(0, player.Health);
		Assert.True(player.IsDead);
	}

	[Fact]
	public void NamesHaveBeenSet()
	{
		foreach (Type derivedType in FindCharacterTypes())
		{
			this._testOutputHelper.WriteLine($"Testing name for type '{derivedType.Name}'...");
			
			Character? character = Activator.CreateInstance(derivedType) as Character;
			Assert.NotNull(character);
			Assert.NotEqual(Character.DefaultName, character.Name);
		}
	}
	
	
	private static IEnumerable<Type> FindCharacterTypes()
	{
		Type baseType = typeof(Character);
		return baseType.Assembly.GetTypes().Where(
			t => t != baseType && t != typeof(Enemy) && baseType.IsAssignableFrom(t)
		);
	}
}
